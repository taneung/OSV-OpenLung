# Contributing
## Welcome!
Welcome to the Open Source Ventilator Project.  We are always very happy to have contributions, whether for trivial cleanups or big new features.
This project can use your help! We need help designing mechanical parts, electronics, firmware and user experience.
We also strongly value documentation and gladly accept improvements to the documentation.

## How To Submit Changes:

We use the fork/merge process, rather than maintaining multiple protected branches. If you're not sure what that means, it's okay, feel free to reach out to us via [slack](https://join.slack.com/t/osventilator/shared_invite/zt-cst4dhk7-BFNMz_vyBPthjlBFYV1yWA).

### Quick Process:

   - Fork the repo to your own GitLab account.
   - Clone your repo to your local device, making changes there.
   - Push any changes you make to your forked repo on GitLab.
   - Request a merge with the main repo and it will be reviewed by a member of the team.

### Resources for GitLab:
    - [GitLab Training Path](https://about.gitlab.com/training/#gitlab)
    - [How to Submit a Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)


## Short Links to Important Resources:

   - docs: requirements / roadmap
   - bugs: issue tracker / bug report tool
   - comms: [OSV Slack][slack]

[slack]: https://join.slack.com/t/osventilator/shared_invite/zt-cst4dhk7-BFNMz_vyBPthjlBFYV1yWA

## Testing

Testing: how to test software components

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Attribution

Parts of this doc where modified from the [Good-CONTRIBUTING.md-template.md][gist-good-contributing] and [Mozilla-Science-Lab][mozilla-science-lab-url]

[gist-good-contributing]: https://gist.github.com/PurpleBooth/b24679402957c63ec426
[mozilla-science-lab-url]: https://mozillascience.github.io/working-open-workshop/contributing/
